(This GCode script was designed to adjust the Z height of a CNC machine according)
(to the minute variations in the surface height in order to achieve a better result in the milling/etching process)
(This script is the output of AutoLevellerAE, 0.9.3u1 Changeset: ...7c35ab @ http://autoleveller.co.uk)
(Author: James Hawthorne PhD. File creation date: 25-11-2017 15:13)
(This program and any of its output is licensed under GPLv2 and as such...)
(AutoLevellerAE comes with ABSOLUTELY NO WARRANTY; for details, see sections 11 and 12 of the GPLv2 @ http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

(The following is a checklist which can be used before the probe routine starts)
(The checklist may not be entirely correct given your machine, therefore you should make your own adaptations as appropriate)
(1. Make sure wires/clips are attached and the probe is working correctly)
(Test probe connections within control software before starting)
(2. Home all axis)
(3. Load GCode file that contains the probe routine into your software controller)
(4. Jog tool tip close to surface and touch off Z)
(5. Jog Z up a fraction to make sure surface is cleared)
(6. Jog to and touch off X and Y at the desired bottom left corner of the probe area)
(Note: The first probe will touch off Z to 0.0 when it first touches to the surface,)
(all other probe values are relative to this first point)

G90 G21 S20000 G17

M0 (Attach probe wires and clips that need attaching)
(Initialize probe routine)
G0 Z25 (Move clear of the board first)
G1 X0.344 Y-2.003 F400 (Move to bottom left corner)
G0 Z2 (Quick move to probe clearance height)
G31 Z-1 F100 (Probe to a maximum of the specified probe height at the specified feed rate)
G92 Z0 (Touch off Z to 0 once contact is made)
G0 Z2 (Move Z to above the contact point)
G31 Z-1 F50 (Repeat at a more accurate slower rate)
G92 Z0
G0 Z2

M40 (Begins a probe log file, when the window appears, enter a name for the log file such as "RawProbeLog.txt")
G0 Z2
G1 X0.344 Y-2.003 F400
G31 Z-1 F100
G0 Z2
G1 X10.57 Y-2.003 F400
G31 Z-1 F100
G0 Z2
G1 X20.796 Y-2.003 F400
G31 Z-1 F100
G0 Z2
G1 X31.022 Y-2.003 F400
G31 Z-1 F100
G0 Z2
G1 X41.248 Y-2.003 F400
G31 Z-1 F100
G0 Z2
G1 X51.474 Y-2.003 F400
G31 Z-1 F100
G0 Z2
G1 X61.7 Y-2.003 F400
G31 Z-1 F100
G0 Z2
G1 X71.926 Y-2.003 F400
G31 Z-1 F100
G0 Z2
G1 X82.152 Y-2.003 F400
G31 Z-1 F100
G0 Z2
G1 X92.378 Y-2.003 F400
G31 Z-1 F100
G0 Z2
G1 X102.604 Y-2.003 F400
G31 Z-1 F100
G0 Z2
G1 X112.83 Y-2.003 F400
G31 Z-1 F100
G0 Z2
G1 X123.056 Y-2.003 F400
G31 Z-1 F100
G0 Z2
G1 X133.282 Y-2.003 F400
G31 Z-1 F100
G0 Z2
G1 X133.282 Y8.42729 F400
G31 Z-1 F100
G0 Z2
G1 X123.056 Y8.42729 F400
G31 Z-1 F100
G0 Z2
G1 X112.83 Y8.42729 F400
G31 Z-1 F100
G0 Z2
G1 X102.604 Y8.42729 F400
G31 Z-1 F100
G0 Z2
G1 X92.378 Y8.42729 F400
G31 Z-1 F100
G0 Z2
G1 X82.152 Y8.42729 F400
G31 Z-1 F100
G0 Z2
G1 X71.926 Y8.42729 F400
G31 Z-1 F100
G0 Z2
G1 X61.7 Y8.42729 F400
G31 Z-1 F100
G0 Z2
G1 X51.474 Y8.42729 F400
G31 Z-1 F100
G0 Z2
G1 X41.248 Y8.42729 F400
G31 Z-1 F100
G0 Z2
G1 X31.022 Y8.42729 F400
G31 Z-1 F100
G0 Z2
G1 X20.796 Y8.42729 F400
G31 Z-1 F100
G0 Z2
G1 X10.57 Y8.42729 F400
G31 Z-1 F100
G0 Z2
G1 X0.344 Y8.42729 F400
G31 Z-1 F100
G0 Z2
G1 X0.344 Y18.85758 F400
G31 Z-1 F100
G0 Z2
G1 X10.57 Y18.85758 F400
G31 Z-1 F100
G0 Z2
G1 X20.796 Y18.85758 F400
G31 Z-1 F100
G0 Z2
G1 X31.022 Y18.85758 F400
G31 Z-1 F100
G0 Z2
G1 X41.248 Y18.85758 F400
G31 Z-1 F100
G0 Z2
G1 X51.474 Y18.85758 F400
G31 Z-1 F100
G0 Z2
G1 X61.7 Y18.85758 F400
G31 Z-1 F100
G0 Z2
G1 X71.926 Y18.85758 F400
G31 Z-1 F100
G0 Z2
G1 X82.152 Y18.85758 F400
G31 Z-1 F100
G0 Z2
G1 X92.378 Y18.85758 F400
G31 Z-1 F100
G0 Z2
G1 X102.604 Y18.85758 F400
G31 Z-1 F100
G0 Z2
G1 X112.83 Y18.85758 F400
G31 Z-1 F100
G0 Z2
G1 X123.056 Y18.85758 F400
G31 Z-1 F100
G0 Z2
G1 X133.282 Y18.85758 F400
G31 Z-1 F100
G0 Z2
G1 X133.282 Y29.28787 F400
G31 Z-1 F100
G0 Z2
G1 X123.056 Y29.28787 F400
G31 Z-1 F100
G0 Z2
G1 X112.83 Y29.28787 F400
G31 Z-1 F100
G0 Z2
G1 X102.604 Y29.28787 F400
G31 Z-1 F100
G0 Z2
G1 X92.378 Y29.28787 F400
G31 Z-1 F100
G0 Z2
G1 X82.152 Y29.28787 F400
G31 Z-1 F100
G0 Z2
G1 X71.926 Y29.28787 F400
G31 Z-1 F100
G0 Z2
G1 X61.7 Y29.28787 F400
G31 Z-1 F100
G0 Z2
G1 X51.474 Y29.28787 F400
G31 Z-1 F100
G0 Z2
G1 X41.248 Y29.28787 F400
G31 Z-1 F100
G0 Z2
G1 X31.022 Y29.28787 F400
G31 Z-1 F100
G0 Z2
G1 X20.796 Y29.28787 F400
G31 Z-1 F100
G0 Z2
G1 X10.57 Y29.28787 F400
G31 Z-1 F100
G0 Z2
G1 X0.344 Y29.28787 F400
G31 Z-1 F100
G0 Z2
G1 X0.344 Y39.71816 F400
G31 Z-1 F100
G0 Z2
G1 X10.57 Y39.71816 F400
G31 Z-1 F100
G0 Z2
G1 X20.796 Y39.71816 F400
G31 Z-1 F100
G0 Z2
G1 X31.022 Y39.71816 F400
G31 Z-1 F100
G0 Z2
G1 X41.248 Y39.71816 F400
G31 Z-1 F100
G0 Z2
G1 X51.474 Y39.71816 F400
G31 Z-1 F100
G0 Z2
G1 X61.7 Y39.71816 F400
G31 Z-1 F100
G0 Z2
G1 X71.926 Y39.71816 F400
G31 Z-1 F100
G0 Z2
G1 X82.152 Y39.71816 F400
G31 Z-1 F100
G0 Z2
G1 X92.378 Y39.71816 F400
G31 Z-1 F100
G0 Z2
G1 X102.604 Y39.71816 F400
G31 Z-1 F100
G0 Z2
G1 X112.83 Y39.71816 F400
G31 Z-1 F100
G0 Z2
G1 X123.056 Y39.71816 F400
G31 Z-1 F100
G0 Z2
G1 X133.282 Y39.71816 F400
G31 Z-1 F100
G0 Z2
G1 X133.282 Y50.14845 F400
G31 Z-1 F100
G0 Z2
G1 X123.056 Y50.14845 F400
G31 Z-1 F100
G0 Z2
G1 X112.83 Y50.14845 F400
G31 Z-1 F100
G0 Z2
G1 X102.604 Y50.14845 F400
G31 Z-1 F100
G0 Z2
G1 X92.378 Y50.14845 F400
G31 Z-1 F100
G0 Z2
G1 X82.152 Y50.14845 F400
G31 Z-1 F100
G0 Z2
G1 X71.926 Y50.14845 F400
G31 Z-1 F100
G0 Z2
G1 X61.7 Y50.14845 F400
G31 Z-1 F100
G0 Z2
G1 X51.474 Y50.14845 F400
G31 Z-1 F100
G0 Z2
G1 X41.248 Y50.14845 F400
G31 Z-1 F100
G0 Z2
G1 X31.022 Y50.14845 F400
G31 Z-1 F100
G0 Z2
G1 X20.796 Y50.14845 F400
G31 Z-1 F100
G0 Z2
G1 X10.57 Y50.14845 F400
G31 Z-1 F100
G0 Z2
G1 X0.344 Y50.14845 F400
G31 Z-1 F100
G0 Z2
G1 X0.344 Y60.57874 F400
G31 Z-1 F100
G0 Z2
G1 X10.57 Y60.57874 F400
G31 Z-1 F100
G0 Z2
G1 X20.796 Y60.57874 F400
G31 Z-1 F100
G0 Z2
G1 X31.022 Y60.57874 F400
G31 Z-1 F100
G0 Z2
G1 X41.248 Y60.57874 F400
G31 Z-1 F100
G0 Z2
G1 X51.474 Y60.57874 F400
G31 Z-1 F100
G0 Z2
G1 X61.7 Y60.57874 F400
G31 Z-1 F100
G0 Z2
G1 X71.926 Y60.57874 F400
G31 Z-1 F100
G0 Z2
G1 X82.152 Y60.57874 F400
G31 Z-1 F100
G0 Z2
G1 X92.378 Y60.57874 F400
G31 Z-1 F100
G0 Z2
G1 X102.604 Y60.57874 F400
G31 Z-1 F100
G0 Z2
G1 X112.83 Y60.57874 F400
G31 Z-1 F100
G0 Z2
G1 X123.056 Y60.57874 F400
G31 Z-1 F100
G0 Z2
G1 X133.282 Y60.57874 F400
G31 Z-1 F100
G0 Z2
G1 X133.282 Y71.00903 F400
G31 Z-1 F100
G0 Z2
G1 X123.056 Y71.00903 F400
G31 Z-1 F100
G0 Z2
G1 X112.83 Y71.00903 F400
G31 Z-1 F100
G0 Z2
G1 X102.604 Y71.00903 F400
G31 Z-1 F100
G0 Z2
G1 X92.378 Y71.00903 F400
G31 Z-1 F100
G0 Z2
G1 X82.152 Y71.00903 F400
G31 Z-1 F100
G0 Z2
G1 X71.926 Y71.00903 F400
G31 Z-1 F100
G0 Z2
G1 X61.7 Y71.00903 F400
G31 Z-1 F100
G0 Z2
G1 X51.474 Y71.00903 F400
G31 Z-1 F100
G0 Z2
G1 X41.248 Y71.00903 F400
G31 Z-1 F100
G0 Z2
G1 X31.022 Y71.00903 F400
G31 Z-1 F100
G0 Z2
G1 X20.796 Y71.00903 F400
G31 Z-1 F100
G0 Z2
G1 X10.57 Y71.00903 F400
G31 Z-1 F100
G0 Z2
G1 X0.344 Y71.00903 F400
G31 Z-1 F100
G0 Z2
G1 X0.344 Y81.43932 F400
G31 Z-1 F100
G0 Z2
G1 X10.57 Y81.43932 F400
G31 Z-1 F100
G0 Z2
G1 X20.796 Y81.43932 F400
G31 Z-1 F100
G0 Z2
G1 X31.022 Y81.43932 F400
G31 Z-1 F100
G0 Z2
G1 X41.248 Y81.43932 F400
G31 Z-1 F100
G0 Z2
G1 X51.474 Y81.43932 F400
G31 Z-1 F100
G0 Z2
G1 X61.7 Y81.43932 F400
G31 Z-1 F100
G0 Z2
G1 X71.926 Y81.43932 F400
G31 Z-1 F100
G0 Z2
G1 X82.152 Y81.43932 F400
G31 Z-1 F100
G0 Z2
G1 X92.378 Y81.43932 F400
G31 Z-1 F100
G0 Z2
G1 X102.604 Y81.43932 F400
G31 Z-1 F100
G0 Z2
G1 X112.83 Y81.43932 F400
G31 Z-1 F100
G0 Z2
G1 X123.056 Y81.43932 F400
G31 Z-1 F100
G0 Z2
G1 X133.282 Y81.43932 F400
G31 Z-1 F100
G0 Z2
G1 X133.282 Y91.86961 F400
G31 Z-1 F100
G0 Z2
G1 X123.056 Y91.86961 F400
G31 Z-1 F100
G0 Z2
G1 X112.83 Y91.86961 F400
G31 Z-1 F100
G0 Z2
G1 X102.604 Y91.86961 F400
G31 Z-1 F100
G0 Z2
G1 X92.378 Y91.86961 F400
G31 Z-1 F100
G0 Z2
G1 X82.152 Y91.86961 F400
G31 Z-1 F100
G0 Z2
G1 X71.926 Y91.86961 F400
G31 Z-1 F100
G0 Z2
G1 X61.7 Y91.86961 F400
G31 Z-1 F100
G0 Z2
G1 X51.474 Y91.86961 F400
G31 Z-1 F100
G0 Z2
G1 X41.248 Y91.86961 F400
G31 Z-1 F100
G0 Z2
G1 X31.022 Y91.86961 F400
G31 Z-1 F100
G0 Z2
G1 X20.796 Y91.86961 F400
G31 Z-1 F100
G0 Z2
G1 X10.57 Y91.86961 F400
G31 Z-1 F100
G0 Z2
G1 X0.344 Y91.86961 F400
G31 Z-1 F100
G0 Z2
G1 X0.344 Y102.2999 F400
G31 Z-1 F100
G0 Z2
G1 X10.57 Y102.2999 F400
G31 Z-1 F100
G0 Z2
G1 X20.796 Y102.2999 F400
G31 Z-1 F100
G0 Z2
G1 X31.022 Y102.2999 F400
G31 Z-1 F100
G0 Z2
G1 X41.248 Y102.2999 F400
G31 Z-1 F100
G0 Z2
G1 X51.474 Y102.2999 F400
G31 Z-1 F100
G0 Z2
G1 X61.7 Y102.2999 F400
G31 Z-1 F100
G0 Z2
G1 X71.926 Y102.2999 F400
G31 Z-1 F100
G0 Z2
G1 X82.152 Y102.2999 F400
G31 Z-1 F100
G0 Z2
G1 X92.378 Y102.2999 F400
G31 Z-1 F100
G0 Z2
G1 X102.604 Y102.2999 F400
G31 Z-1 F100
G0 Z2
G1 X112.83 Y102.2999 F400
G31 Z-1 F100
G0 Z2
G1 X123.056 Y102.2999 F400
G31 Z-1 F100
G0 Z2
G1 X133.282 Y102.2999 F400
G31 Z-1 F100
G0 Z2
G1 X133.282 Y112.73019 F400
G31 Z-1 F100
G0 Z2
G1 X123.056 Y112.73019 F400
G31 Z-1 F100
G0 Z2
G1 X112.83 Y112.73019 F400
G31 Z-1 F100
G0 Z2
G1 X102.604 Y112.73019 F400
G31 Z-1 F100
G0 Z2
G1 X92.378 Y112.73019 F400
G31 Z-1 F100
G0 Z2
G1 X82.152 Y112.73019 F400
G31 Z-1 F100
G0 Z2
G1 X71.926 Y112.73019 F400
G31 Z-1 F100
G0 Z2
G1 X61.7 Y112.73019 F400
G31 Z-1 F100
G0 Z2
G1 X51.474 Y112.73019 F400
G31 Z-1 F100
G0 Z2
G1 X41.248 Y112.73019 F400
G31 Z-1 F100
G0 Z2
G1 X31.022 Y112.73019 F400
G31 Z-1 F100
G0 Z2
G1 X20.796 Y112.73019 F400
G31 Z-1 F100
G0 Z2
G1 X10.57 Y112.73019 F400
G31 Z-1 F100
G0 Z2
G1 X0.344 Y112.73019 F400
G31 Z-1 F100
G0 Z2
G1 X0.344 Y123.16048 F400
G31 Z-1 F100
G0 Z2
G1 X10.57 Y123.16048 F400
G31 Z-1 F100
G0 Z2
G1 X20.796 Y123.16048 F400
G31 Z-1 F100
G0 Z2
G1 X31.022 Y123.16048 F400
G31 Z-1 F100
G0 Z2
G1 X41.248 Y123.16048 F400
G31 Z-1 F100
G0 Z2
G1 X51.474 Y123.16048 F400
G31 Z-1 F100
G0 Z2
G1 X61.7 Y123.16048 F400
G31 Z-1 F100
G0 Z2
G1 X71.926 Y123.16048 F400
G31 Z-1 F100
G0 Z2
G1 X82.152 Y123.16048 F400
G31 Z-1 F100
G0 Z2
G1 X92.378 Y123.16048 F400
G31 Z-1 F100
G0 Z2
G1 X102.604 Y123.16048 F400
G31 Z-1 F100
G0 Z2
G1 X112.83 Y123.16048 F400
G31 Z-1 F100
G0 Z2
G1 X123.056 Y123.16048 F400
G31 Z-1 F100
G0 Z2
G1 X133.282 Y123.16048 F400
G31 Z-1 F100
G0 Z2
G1 X133.282 Y133.59077 F400
G31 Z-1 F100
G0 Z2
G1 X123.056 Y133.59077 F400
G31 Z-1 F100
G0 Z2
G1 X112.83 Y133.59077 F400
G31 Z-1 F100
G0 Z2
G1 X102.604 Y133.59077 F400
G31 Z-1 F100
G0 Z2
G1 X92.378 Y133.59077 F400
G31 Z-1 F100
G0 Z2
G1 X82.152 Y133.59077 F400
G31 Z-1 F100
G0 Z2
G1 X71.926 Y133.59077 F400
G31 Z-1 F100
G0 Z2
G1 X61.7 Y133.59077 F400
G31 Z-1 F100
G0 Z2
G1 X51.474 Y133.59077 F400
G31 Z-1 F100
G0 Z2
G1 X41.248 Y133.59077 F400
G31 Z-1 F100
G0 Z2
G1 X31.022 Y133.59077 F400
G31 Z-1 F100
G0 Z2
G1 X20.796 Y133.59077 F400
G31 Z-1 F100
G0 Z2
G1 X10.57 Y133.59077 F400
G31 Z-1 F100
G0 Z2
G1 X0.344 Y133.59077 F400
G31 Z-1 F100
G0 Z2
G1 X0.344 Y144.02106 F400
G31 Z-1 F100
G0 Z2
G1 X10.57 Y144.02106 F400
G31 Z-1 F100
G0 Z2
G1 X20.796 Y144.02106 F400
G31 Z-1 F100
G0 Z2
G1 X31.022 Y144.02106 F400
G31 Z-1 F100
G0 Z2
G1 X41.248 Y144.02106 F400
G31 Z-1 F100
G0 Z2
G1 X51.474 Y144.02106 F400
G31 Z-1 F100
G0 Z2
G1 X61.7 Y144.02106 F400
G31 Z-1 F100
G0 Z2
G1 X71.926 Y144.02106 F400
G31 Z-1 F100
G0 Z2
G1 X82.152 Y144.02106 F400
G31 Z-1 F100
G0 Z2
G1 X92.378 Y144.02106 F400
G31 Z-1 F100
G0 Z2
G1 X102.604 Y144.02106 F400
G31 Z-1 F100
G0 Z2
G1 X112.83 Y144.02106 F400
G31 Z-1 F100
G0 Z2
G1 X123.056 Y144.02106 F400
G31 Z-1 F100
G0 Z2
G1 X133.282 Y144.02106 F400
G31 Z-1 F100
G0 Z2
M41 (Closes the opened log file)
G0 X0.344 Y-2.003 Z25
M0 (Detach any clips used for probing)
M30
