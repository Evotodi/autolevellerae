package com.cncsoftwaretools.autoleveller.reader.processor;

import com.cncsoftwaretools.autoleveller.reader.GCodeReader;
import com.cncsoftwaretools.autoleveller.reader.PlainGCodeReader;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.*;

public class ReaderWriterTestIT
{
    private ProcessorTestClass writerProcessor;
    private GCodeReader reader;

    @Before
    public void setup() throws FileNotFoundException
    {
        FileReader fileReader = new FileReader("src/test/SerialNoText.ngc");
        reader = PlainGCodeReader.newInstance(fileReader);
        writerProcessor = new ProcessorTestClass();
    }

    @Test
    public void countCalls() throws IOException
    {
        reader.readFile(writerProcessor);

        assertThat(writerProcessor.processStateCount(), equalTo(119));
    }

    @Test
    public void firstAndLast() throws IOException
    {
        GCodeProcessor realWriter = WriterProcessor.newInstance(Optional.<GCodeProcessor>empty(), "src/test/outFile.txt", false);
        reader.readFile(realWriter);

        try(BufferedReader br=new BufferedReader(new FileReader("src/test/outFile.txt"))){
            // the reader now preprocesses each line, i.e. the text is replaced before any processing is done,
            // so % is replaced with (removed % symbol) etc.
            assertThat(br.lines().findFirst().get(), equalTo("(removed % symbol)"));
        }

        try(BufferedReader br=new BufferedReader(new FileReader("src/test/outFile.txt"))){
            // the reader now preprocesses each line, i.e. the text is replaced before any processing is done,
            // so % is replaced with (removed % symbol) etc.
            assertThat(br.lines().reduce((previous, current) -> current).get(), equalTo("(removed % symbol)"));
        }

        try(BufferedReader br=new BufferedReader(new FileReader("src/test/outFile.txt"))){
            assertThat(br.lines().count(), equalTo(119L));
        }

    }

    @Test
    public void makeSureFinishCalledOnce() throws IOException
    {
        GCodeProcessor mockedProcessor = mock(GCodeProcessor.class);

        when(mockedProcessor.nextProcessor()).thenReturn(Optional.empty());
        reader.readFile(mockedProcessor);

        verify(mockedProcessor, times(1)).finish();

    }
}
