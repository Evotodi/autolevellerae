package com.cncsoftwaretools.autoleveller.reader.processor;

import com.cncsoftwaretools.autoleveller.reader.GCodeState;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class ReplacerTest
{
    private ReplacerPreProcessor replacer;

    @Before
    public void setUp() throws Exception
    {
        replacer = ReplacerPreProcessor.newInstance();
    }

    @Test
    public void normalLineNoChange()
    {
        GCodeState previousState = GCodeState.newInstance(Optional.empty(), "G1Z-1.699F762.0");
        GCodeState currentState = GCodeState.newInstance(Optional.of(previousState), "G1X48.092Y-15.843F1524.0");

        GCodeState replacement = replacer.replace(currentState);

        assertThat(replacement, equalTo(currentState));
    }

    @Test
    public void replacePercentageWithComment()
    {
        GCodeState previousState = GCodeState.newInstance(Optional.empty(), "G1Z-1.699F762.0");
        GCodeState currentState = GCodeState.newInstance(Optional.of(previousState), "%");

        GCodeState replacement = replacer.replace(currentState);

        assertThat(replacement, equalTo(GCodeState.newInstance(Optional.of(previousState), "(removed % symbol)")));
    }

    @Test
    public void lineWithPeriodsAfterDigits_ShouldRemoveAllTrailingPeriods()
    {
        GCodeState previousState = GCodeState.newInstance(Optional.empty(), "N75 Z5.");
        GCodeState currentState = GCodeState.newInstance(Optional.of(previousState), "N80 G98 G81 X40. Y16.5 Z-2. R5. F500.");

        GCodeState replacement = replacer.replace(currentState);

        assertThat(replacement, equalTo(GCodeState.newInstance(Optional.of(previousState), "N80 G98 G81 X40 Y16.5 Z-2 R5 F500")));
    }

    @Test
    public void lineWithPeriodsBeforeDigits_ShouldReplaceAllLeadingPeriodsWith0Point()
    {
        GCodeState previousState = GCodeState.newInstance(Optional.empty(), "N75 Z5");
        GCodeState currentState = GCodeState.newInstance(Optional.of(previousState), "N0080 X1.7076 Y.9026 I-.2112 J.3467");

        GCodeState replacement = replacer.replace(currentState);

        assertThat(replacement, equalTo(GCodeState.newInstance(Optional.of(previousState), "N0080 X1.7076 Y0.9026 I-0.2112 J0.3467")));
    }

    @Test
    public void lineWithTrailingPeriodInComment_ShouldNotBeReplaced()
    {
        GCodeState previousState = GCodeState.newInstance(Optional.empty(), "(Copyright 2005 - 2012 by John Johnson)");
        GCodeState currentState = GCodeState.newInstance(Optional.of(previousState), "(See readme.txt for licensing terms.)");

        GCodeState replacement = replacer.replace(currentState);

        assertThat(replacement, equalTo(GCodeState.newInstance(Optional.of(previousState), "(See readme.txt for licensing terms.)")));
    }

    @Test
    public void aMixOfleadingAndTrailingDotsAndComments_shouldReturnLineAlteredAsExpected()
    {
        GCodeState previousState = GCodeState.newInstance(Optional.empty(), "(blah)");
        GCodeState currentState = GCodeState.newInstance(Optional.of(previousState),
                "N3630 G02 X-1.138 Y.6518 I-.1526 J.0234 F20. (a random.commment with .'s in.)");

        assertThat(replacer.replace(currentState), equalTo(GCodeState.newInstance(Optional.of(previousState),
                "N3630 G02 X-1.138 Y0.6518 I-0.1526 J0.0234 F20 (a random.commment with .'s in.)")));
    }

    @Test
    public void doNotAddclosingBracketIfAlreadyExists()
    {
        GCodeState previousState = GCodeState.newInstance(Optional.empty(), "(See readme.txt for licensing terms.)");
        GCodeState currentState = GCodeState.newInstance(Optional.of(previousState), "(Copyright 2005 - 2012 by John Johnson)");

        GCodeState replacement = replacer.replace(currentState);

        assertThat(replacement, equalTo(GCodeState.newInstance(Optional.of(previousState), "(Copyright 2005 - 2012 by John Johnson)")));
    }
}
