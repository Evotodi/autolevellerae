package com.cncsoftwaretools.autoleveller.reader.processor;

import com.cncsoftwaretools.autoleveller.Pair;
import com.cncsoftwaretools.autoleveller.reader.GCodeState;
import com.cncsoftwaretools.autoleveller.reader.WordGroup;
import com.google.common.collect.ImmutableList;
import javafx.geometry.Point2D;
import org.hamcrest.Matchers;
import org.junit.Test;

import static com.cncsoftwaretools.autoleveller.matcher.Point2DMatcher.pointRoundedTo3DP;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class LineSegmentorTest
{
    private LineSegmentProcessor segmentor = LineSegmentProcessor.newInstance(Optional.empty(), 5);

    @Test
    public void noPreviousState()
    {
        GCodeState state = GCodeState.newInstance(Optional.<GCodeState>empty(), "G1 X-22.1314 Y-5.8365 Z-0.147625");

        List<GCodeState> segments = segmentor.segment(state);

        assertThat(segments, contains(state));
    }

    @Test
    public void dontSegmentIfZPositive()
    {
        GCodeState firstState = GCodeState.newInstance(Optional.<GCodeState>empty(), "G1 X-22.1314 Y-5.8365 Z-0.147625");
        GCodeState state = GCodeState.newInstance(Optional.of(firstState), "X-12.1314 Z1");

        segmentor.segment(firstState);
        List<GCodeState> segments = segmentor.segment(state);

        assertThat(segments, contains(state));
    }

    @Test
    public void dontSegmentShortLine()
    {
        segmentor = LineSegmentProcessor.newInstance(Optional.empty(), 15);

        GCodeState previousState = GCodeState.newInstance(Optional.<GCodeState>empty(), "G1 X-12.38344 Y-24.01076 Z-0.147625");
        GCodeState currentState = GCodeState.newInstance(Optional.of(previousState), "G1 X-17.11688 Y-25.62152 Z-0.147625");
        segmentor.segment(previousState);
        List<GCodeState> segments = segmentor.segment(currentState);

        assertThat(segments, contains(currentState));
    }

    @Test
    public void dontSegmentNonLinearMoves()
    {
        GCodeState previousState = GCodeState.newInstance(Optional.<GCodeState>empty(), "G1 X18.5 Y46 Z-1");
        GCodeState state = GCodeState.newInstance(Optional.of(previousState), "G03 X44.5 Y6 Z-2.0I-1.5 J-29.3 F300");

        segmentor.segment(previousState);
        List<GCodeState> segments = segmentor.segment(state);

        assertThat(segments, contains(state));
    }

    @Test
    public void segmentToListTest()
    {
        segmentor = LineSegmentProcessor.newInstance(Optional.empty(), 10);

        GCodeState previousState = GCodeState.newInstance(Optional.<GCodeState>empty(), "G1 X-45 Y-26 Z-0.147625");
        GCodeState currentState = GCodeState.newInstance(Optional.of(previousState), "G1 X55 Y35 Z-0.147625");
        List<GCodeState> segments = segmentor.segment(currentState);

        List<Pair<BigDecimal, BigDecimal>> xAndYPairs = segments.stream()
                .map(gCodeState -> Pair.createPair(gCodeState.getModalWordByGroup(WordGroup.XAXISGROUP).get().getWordPair().getSecond(),
                        gCodeState.getModalWordByGroup(WordGroup.YAXISGROUP).get().getWordPair().getSecond()))
                .collect(toList());

        List<Pair<BigDecimal, BigDecimal>> expectedPoints = new ArrayList<>();

        expectedPoints.add(Pair.createPair(new BigDecimal("-36.46296"), new BigDecimal("-20.79241")));
        expectedPoints.add(Pair.createPair(new BigDecimal("-27.92592"), new BigDecimal("-15.58482")));
        expectedPoints.add(Pair.createPair(new BigDecimal("-19.38888"), new BigDecimal("-10.37723")));
        expectedPoints.add(Pair.createPair(new BigDecimal("-10.85184"), new BigDecimal("-5.16964")));
        expectedPoints.add(Pair.createPair(new BigDecimal("-2.31480"), new BigDecimal("0.03795")));
        expectedPoints.add(Pair.createPair(new BigDecimal("6.22223"), new BigDecimal("5.24554")));
        expectedPoints.add(Pair.createPair(new BigDecimal("14.75926"), new BigDecimal("10.45313")));
        expectedPoints.add(Pair.createPair(new BigDecimal("23.29629"), new BigDecimal("15.66072")));
        expectedPoints.add(Pair.createPair(new BigDecimal("31.83332"), new BigDecimal("20.86832")));
        expectedPoints.add(Pair.createPair(new BigDecimal("40.37036"), new BigDecimal("26.07591")));
        expectedPoints.add(Pair.createPair(new BigDecimal("48.90739"), new BigDecimal("31.28351")));
        expectedPoints.add(Pair.createPair(new BigDecimal("55"), new BigDecimal("35")));

        assertThat(xAndYPairs, equalTo(expectedPoints));
    }

    @Test
    public void segment3D_ShouldNotTryToSegmentPath()
    {
        GCodeState previousState = GCodeState.newInstance(Optional.empty(), "G1 X1 Y2 Z-3");
        GCodeState currentState = GCodeState.newInstance(Optional.of(previousState), "G1 Y14 Z-4");
        segmentor.segment(previousState);
        List<GCodeState> segments = segmentor.segment(currentState);

        assertThat(segments, contains(currentState));
    }

    @Test
    public void segment_reinsertZandF()
    {
        segmentor = LineSegmentProcessor.newInstance(Optional.empty(), 10);

        GCodeState previousState = GCodeState.newInstance(Optional.<GCodeState>empty(), "G1 X-45 Y-26 Z-0.147625");
        GCodeState currentState = GCodeState.newInstance(Optional.of(previousState), "G1 X55 Y35 Z-0.147625 F1000");
        List<GCodeState> segments = segmentor.segment(currentState);

        assertThat(segments.get(0), equalTo(GCodeState.newInstance(Optional.of(previousState), "G1 X-36.46296 Y-20.79241 Z-0.147625 F1000 " +
                ";segmented line. Max segment length set to 10")));
    }

    @Test
    public void segmentNegativeLine()
    {
        GCodeState previousState = GCodeState.newInstance(Optional.<GCodeState>empty(), "G1 X-7.65 Y-22.4 Z-0.1");
        GCodeState currentState = GCodeState.newInstance(Optional.of(previousState), "G1 X-72.3 Y-44.4 Z-0.1");
        List<GCodeState> segments = segmentor.segment(currentState);

        List<Pair<BigDecimal, BigDecimal>> xAndYPairs = segments.stream()
                .map(gCodeState -> Pair.createPair(gCodeState.getModalWordByGroup(WordGroup.XAXISGROUP).get().getWordPair().getSecond(),
                        gCodeState.getModalWordByGroup(WordGroup.YAXISGROUP).get().getWordPair().getSecond()))
                .collect(toList());

        List<Pair<BigDecimal, BigDecimal>> expectedPoints = new ArrayList<>();

        expectedPoints.add(Pair.createPair(new BigDecimal("-12.38344"), new BigDecimal("-24.01076")));
        expectedPoints.add(Pair.createPair(new BigDecimal("-17.11688"), new BigDecimal("-25.62152")));
        expectedPoints.add(Pair.createPair(new BigDecimal("-21.85032"), new BigDecimal("-27.23228")));
        expectedPoints.add(Pair.createPair(new BigDecimal("-26.58376"), new BigDecimal("-28.84304")));
        expectedPoints.add(Pair.createPair(new BigDecimal("-31.31720"), new BigDecimal("-30.45380")));
        expectedPoints.add(Pair.createPair(new BigDecimal("-36.05064"), new BigDecimal("-32.06456")));
        expectedPoints.add(Pair.createPair(new BigDecimal("-40.78408"), new BigDecimal("-33.67532")));
        expectedPoints.add(Pair.createPair(new BigDecimal("-45.51752"), new BigDecimal("-35.28608")));
        expectedPoints.add(Pair.createPair(new BigDecimal("-50.25096"), new BigDecimal("-36.89684")));
        expectedPoints.add(Pair.createPair(new BigDecimal("-54.98440"), new BigDecimal("-38.50760")));
        expectedPoints.add(Pair.createPair(new BigDecimal("-59.71784"), new BigDecimal("-40.11836")));
        expectedPoints.add(Pair.createPair(new BigDecimal("-64.45128"), new BigDecimal("-41.72912")));
        expectedPoints.add(Pair.createPair(new BigDecimal("-69.18472"), new BigDecimal("-43.33989")));
        expectedPoints.add(Pair.createPair(new BigDecimal("-72.3"), new BigDecimal("-44.4")));

        assertThat(xAndYPairs, equalTo(expectedPoints));
    }

    @Test
    public void lastProcessedStateIncludesSegmentedState()
    {
        ProcessorTestClass processor = new ProcessorTestClass();
        segmentor = LineSegmentProcessor.newInstance(Optional.of(processor), 5);

        GCodeState previousState = GCodeState.newInstance(Optional.<GCodeState>empty(), "G1 X-7.65 Y-22.4 Z-0.1");
        GCodeState currentState = GCodeState.newInstance(Optional.of(previousState), "G1 X-72.3 Y-44.4 Z-0.1");
        GCodeState singleState = GCodeState.newInstance(Optional.of(currentState), "G1 X-73 Y-46 Z-0.1");
        segmentor.interpret(ImmutableList.of(currentState));
        segmentor.interpret(ImmutableList.of(singleState));

        assertThat(processor.processStateCount(), equalTo(15));
        assertThat(processor.getLastState().getIteration(), equalTo(16));
    }

    @Test
    public void lineShouldBeSegmented()
    {
        GCodeState firstState = GCodeState.newInstance(Optional.<GCodeState>empty(), "G1 X-22.1314 Y-5.8365 Z-1");
        GCodeState newState = GCodeState.newInstance(Optional.of(firstState), "X-12.13140 Z-1");

        List<GCodeState> segments = segmentor.segment(newState);
        GCodeState segmentedState = GCodeState.newInstance(Optional.of(firstState), "X-17.13140 Z-1 ;segmented line. Max segment length set to 5");
        GCodeState state = GCodeState.newInstance(Optional.of(segmentedState), "X-12.13140 Z-1");

        assertThat(segments, contains(segmentedState, state));
    }

    @Test
    public void canSegment_notAG0orG1()
    {
        GCodeState state1 = GCodeState.newInstance(Optional.empty(), "G00 Z2.5400");
        GCodeState state2 = GCodeState.newInstance(Optional.of(state1), "G82 X-24.1300 Y32.0675 Z-0.1000 F508.00 R2.5400 P1.000000");
        GCodeState state3 = GCodeState.newInstance(Optional.of(state2), "G82 X-24.1300 Y33.9725");
        GCodeState state4 = GCodeState.newInstance(Optional.of(state3), "G82 X-20.3200 Y24.4475");
        GCodeState state5 = GCodeState.newInstance(Optional.of(state4), "G82 X-20.3200 Y26.3525");

        assertThat(segmentor.canSegment(state1), is(false));
        assertThat(segmentor.canSegment(state2), is(false));
        assertThat(segmentor.canSegment(state3), is(false));
        assertThat(segmentor.canSegment(state4), is(false));
        assertThat(segmentor.canSegment(state5), is(false));
    }

    @Test
    public void getPointFromState()
    {
        GCodeState firstState = GCodeState.newInstance(Optional.<GCodeState>empty(), "G1 X-22.1314 Y-5.8365 Z-0.147625");
        Optional<Point2D> xAndY = firstState.get2DPoint();

        assertThat(xAndY.get().getX(), equalTo(-22.1314));
        assertThat(xAndY.get().getY(), equalTo(-5.8365));
    }

    @Test
    public void emptyPoints()
    {
        GCodeState firstState = GCodeState.newInstance(Optional.<GCodeState>empty(), "G1 X-22.1314 Z-0.147625");
        Optional<Point2D> noY = firstState.get2DPoint();
        Optional<Point2D> noPrevious = firstState.getPreviousState().orElse(
                GCodeState.newInstance(Optional.empty(), "")).get2DPoint();

        assertThat(noY, Matchers.equalTo(Optional.<Pair<BigDecimal, BigDecimal>>empty()));
        assertThat(noPrevious, Matchers.equalTo(Optional.<Pair<BigDecimal, BigDecimal>>empty()));
    }

    @Test
    public void segmentLineFirstSegment()
    {
        segmentor = LineSegmentProcessor.newInstance(Optional.<GCodeProcessor>empty(), 10);

        Point2D initialPoint = new Point2D(-45, -26);
        Point2D endPoint = new Point2D(55, 35);

        Point2D firstSegmentedPoint = segmentor.reduceSegmentLength(initialPoint, endPoint);

        assertThat(firstSegmentedPoint, pointRoundedTo3DP(new Point2D(-36.463, -20.792)));

    }

    @Test
    public void segmentLineSecondSegment()
    {
        segmentor = LineSegmentProcessor.newInstance(Optional.<GCodeProcessor>empty(), 10);

        Point2D initialPoint = new Point2D(-36.46296, -20.79241);
        Point2D endPoint = new Point2D(55, 35);

        Point2D firstSegmentedPoint = segmentor.reduceSegmentLength(initialPoint, endPoint);

        assertThat(firstSegmentedPoint, pointRoundedTo3DP(new Point2D(-27.926, -15.585)));

    }

    @Test
    public void segmentNegativeLineFirstSegment()
    {
        Point2D initialPoint = new Point2D(-7.65, -22.4);
        Point2D endPoint = new Point2D(-72.3, -44.4);

        Point2D firstSegmentedPoint = segmentor.reduceSegmentLength(initialPoint, endPoint);

        assertThat(firstSegmentedPoint, pointRoundedTo3DP(new Point2D(-12.383, -24.011)));

    }

    @Test
    public void segmentNegativeLineSecondSegment()
    {
        Point2D initialPoint = new Point2D(-12.38344, -24.01076);
        Point2D endPoint = new Point2D(-72.3, -44.4);

        Point2D firstSegmentedPoint = segmentor.reduceSegmentLength(initialPoint, endPoint);

        assertThat(firstSegmentedPoint, pointRoundedTo3DP(new Point2D(-17.117, -25.622)));

    }

}
