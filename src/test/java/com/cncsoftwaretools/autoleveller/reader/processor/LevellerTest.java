package com.cncsoftwaretools.autoleveller.reader.processor;

import com.cncsoftwaretools.autoleveller.*;
import com.cncsoftwaretools.autoleveller.reader.GCodeState;
import com.google.common.collect.ImmutableList;
import javafx.geometry.Point2D;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class LevellerTest
{
    private LevelerProcessor leveller;
    private Mesh mesh;
    private ProcessorTestClass postLeveller;
    private Function<BigDecimal, String> modifyZReplacer = originalZ -> "Z[#100+" + originalZ + ']';

    @Before
    public void setUp() throws Exception
    {
        mesh = new Mesh.Builder(Units.INCHES, Controller.TURBOCNC, new BigDecimal("-65.396"), new BigDecimal("-58.695"),
                new BigDecimal("125.562"), new BigDecimal("100.541"))
                .buildSpacing(14.5) // ySpaceSize = 16.75683, xSpaceSize = 15.69525
                .build();

        postLeveller = new ProcessorTestClass();
        leveller = LevelerProcessor.newInstance(Optional.of(postLeveller), mesh);
    }

    @Test
    public void getLevellingStrings()
    {
        GCodeState state = GCodeState.newInstance(Optional.<GCodeState>empty(), "N220X0Y0F1200");
        state = GCodeState.newInstance(Optional.of(state), "N230G0X-22.326Y4.32Z1");
        state = GCodeState.newInstance(Optional.of(state), "N240G1Z-0.6 F1200");

        List<String> levellingStrings = leveller.interpolateState(state);

        assertThat(levellingStrings.get(0), equalTo("#102=[#538+0.23944*#529-0.23944*#538]"));
        assertThat(levellingStrings.get(1), equalTo("#101=[#539+0.23944*#530-0.23944*#539]"));
        assertThat(levellingStrings.get(2), equalTo("#100=[#102+0.74414*#101-0.74414*#102]"));
        assertThat(levellingStrings.get(3), equalTo("N240G1Z[#100+-0.6] F1200"));
    }

    @Test
    public void levellingAnArc_shouldOnlyModifyZNothingElse()
    {
        GCodeState zeroState = GCodeState.newInstance(Optional.empty(), "N545 G90.1");
        GCodeState firstState = GCodeState.newInstance(Optional.of(zeroState), "N550 G1 X-7 Y33 Z-2 F3000");
        GCodeState blankState = GCodeState.newInstance(Optional.of(firstState), "");
        GCodeState arcState = GCodeState.newInstance(Optional.of(blankState), "N555 G2 X42.9 Y32 I17 J16.7 F300");

        List<String> levellingStrings = leveller.interpolateState(arcState);

        assertThat(levellingStrings.get(0), equalTo("#102=[#560+0.58758*#551-0.58758*#560]"));
        assertThat(levellingStrings.get(1), equalTo("#101=[#561+0.58758*#552-0.58758*#561]"));
        assertThat(levellingStrings.get(2), equalTo("#100=[#102+0.89992*#101-0.89992*#102]"));
        assertThat(levellingStrings.get(3), equalTo("N555 G2 X42.9 Y32Z[#100+-2] I17 J16.7 F300"));
    }

    @Test
    public void getLevellingStates()
    {
        GCodeState state = GCodeState.newInstance(Optional.<GCodeState>empty(), "N220X0Y0F1200");
        state = GCodeState.newInstance(Optional.of(state), "N230G0X-22.326Y4.32Z1");
        state = GCodeState.newInstance(Optional.of(state), "N240G1Z-0.6 F1200");

        leveller.interpret(ImmutableList.of(state));
        GCodeState latestState = postLeveller.getLastState();

        assertThat(postLeveller.processStateCount(), is(4));
        assertThat(latestState.getIteration(), is(6));
        assertThat(latestState.getRawLine(), equalTo("N240G1Z[#100+-0.6] F1200"));
    }

    @Test
    public void getLevellingStringsOutsideMesh()
    {
        GCodeState state = GCodeState.newInstance(Optional.<GCodeState>empty(), "N220X0Y0F1200");
        state = GCodeState.newInstance(Optional.of(state), "N230G0X-22.326Y4.32Z1");
        state = GCodeState.newInstance(Optional.of(state), "N240G1X-70Y105Z-0.6 F1200");

        List<String> levellingStrings = leveller.interpolateState(state);

        assertThat(levellingStrings.get(0), equalTo("#102=[#554+0*#554-0*#554]"));
        assertThat(levellingStrings.get(1), equalTo("#101=[#554+0*#554-0*#554]"));
        assertThat(levellingStrings.get(2), equalTo("#100=[#102+0*#101-0*#102]"));
        assertThat(levellingStrings.get(3), equalTo("N240G1X-70Y105Z[#100+-0.6] F1200"));
    }

    @Test
    public void getLevellingStringsZHigh()
    {
        GCodeState state = GCodeState.newInstance(Optional.<GCodeState>empty(), "N220X0Y0F1200");
        state = GCodeState.newInstance(Optional.of(state), "N230G0X-22.326Y4.32Z1");
        state = GCodeState.newInstance(Optional.of(state), "N240G1X-70Y105Z0 F1200");

        List<String> levellingStrings = leveller.interpolateState(state);

        assertThat(levellingStrings.size(), equalTo(1));
        assertThat(levellingStrings.get(0), equalTo("N240G1X-70Y105Z0 F1200"));
    }

    @Test
    public void interpolationString()
    {
        Point2D pointToFind = new Point2D(-22.326, 4.32);
        List<ThreeDPoint> surroundingPoints = mesh.getSurroundingPoints(pointToFind); //TL=0, TR=1, BL=2, BR=3
        Pair<BigDecimal, String> topLeftYnZ = Pair.createPair(surroundingPoints.get(0).getYValue(), surroundingPoints.get(0).getZValue());
        Pair<BigDecimal, String> bottomLeftYnZ = Pair.createPair(surroundingPoints.get(2).getYValue(), surroundingPoints.get(2).getZValue());

        String levelledString = leveller.interpolateToStringFormula(new BigDecimal("4.32"), topLeftYnZ, bottomLeftYnZ);

        assertThat(levelledString, equalTo("#538+0.23944*#529-0.23944*#538"));
    }

    @Test
    public void interpolationStringForX()
    {
        Point2D pointToFind = new Point2D(-22.326, 4.32);
        List<ThreeDPoint> surroundingPoints = mesh.getSurroundingPoints(pointToFind); //TL=0, TR=1, BL=2, BR=3
        Pair<BigDecimal, String> topLeftXnZ = Pair.createPair(surroundingPoints.get(0).getXValue(), "#102");
        Pair<BigDecimal, String> topRightXnZ = Pair.createPair(surroundingPoints.get(1).getXValue(), "#101");

        String levelledString = leveller.interpolateToStringFormula(new BigDecimal("-22.326"), topLeftXnZ, topRightXnZ);

        assertThat(levelledString, equalTo("#102+0.74414*#101-0.74414*#102"));
    }

    @Test
    public void modifyZLineRegardless()
    {
        GCodeState state = GCodeState.newInstance(Optional.<GCodeState>empty(), "N220X0Y0F1200");
        state = GCodeState.newInstance(Optional.of(state), "N230G0X-22.326Y4.32Z1");
        state = GCodeState.newInstance(Optional.of(state), "N240G4Z6F1200");

        String levelledLine = leveller.modifyZ(state, modifyZReplacer);

        assertThat(levelledLine, equalTo("N240G4Z[#100+6]F1200"));
    }

    @Test
    public void modifyZCommentWithZHasNoEffect()
    {
        GCodeState state = GCodeState.newInstance(Optional.<GCodeState>empty(), "N220X0Y0F1200");
        state = GCodeState.newInstance(Optional.of(state), "N230G0X-22.326Y4.32Z1");
        state = GCodeState.newInstance(Optional.of(state), "(N240G4Z6F1200)");

        String levelledLine = leveller.modifyZ(state, modifyZReplacer);

        assertThat(levelledLine, equalTo("(N240G4Z6F1200)"));
    }

    @Test
    public void modifyZNoZNoEffect()
    {
        GCodeState state = GCodeState.newInstance(Optional.<GCodeState>empty(), "N220X0Y0F1200");
        state = GCodeState.newInstance(Optional.of(state), "N230G0X-22.326Y4.32Z1");
        state = GCodeState.newInstance(Optional.of(state), "N240G9F1200");

        String levelledLine = leveller.modifyZ(state, modifyZReplacer);

        assertThat(levelledLine, equalTo("N240G9F1200"));
    }

    @Test
    public void modifyZonlyXandY()
    {
        GCodeState state = GCodeState.newInstance(Optional.<GCodeState>empty(), "N220X0Y0F1200");
        state = GCodeState.newInstance(Optional.of(state), "N230G0X-22.326Y4.32Z1");
        state = GCodeState.newInstance(Optional.of(state), "N240G9X1F1200");

        String levelledLine = leveller.modifyZ(state, modifyZReplacer);

        assertThat(levelledLine, equalTo("N240G9X1Z[#100+1]F1200"));
    }

    @Test
    public void modifyZignoreComments()
    {
        GCodeState state = GCodeState.newInstance(Optional.<GCodeState>empty(), "N220X0Y0F1200");
        state = GCodeState.newInstance(Optional.of(state), "N230G0X-22.326Y4.32Z1");
        state = GCodeState.newInstance(Optional.of(state), "N240G9X1Y2Z3F1200(a random Z4.5 in comment);another comment with Z3.4");

        String levelledLine = leveller.modifyZ(state, modifyZReplacer);

        assertThat(levelledLine, equalTo("N240G9X1Y2Z[#100+3]F1200(a random Z4.5 in comment);another comment with Z3.4"));
    }
}
