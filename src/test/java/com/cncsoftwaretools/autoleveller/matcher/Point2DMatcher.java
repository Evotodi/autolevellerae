package com.cncsoftwaretools.autoleveller.matcher;

import javafx.geometry.Point2D;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import java.math.BigDecimal;

public class Point2DMatcher
{
	public static Matcher<Point2D> pointRoundedTo3DP(final Point2D expectedPoint)
	{
		return new TypeSafeMatcher<Point2D>()
        {
            private final Point2D roundedExpectedPoint;
            private Point2D roundedPoint = new Point2D(-1, -1);

            {
                BigDecimal expectedPointX = new BigDecimal(expectedPoint.getX());
                expectedPointX = expectedPointX.setScale(3, BigDecimal.ROUND_HALF_UP);
                BigDecimal expectedPointY = new BigDecimal(expectedPoint.getY());
                expectedPointY = expectedPointY.setScale(3, BigDecimal.ROUND_HALF_UP);
                roundedExpectedPoint = new Point2D(expectedPointX.doubleValue(), expectedPointY.doubleValue());
            }

			@Override
			public void describeTo(final Description pointDescription) 
			{
				pointDescription.appendText(roundedExpectedPoint.toString());
			}

			@Override
			protected boolean matchesSafely(final Point2D point)
			{
                BigDecimal pointX = new BigDecimal(point.getX());
                pointX = pointX.setScale(3, BigDecimal.ROUND_HALF_UP);
                BigDecimal pointY = new BigDecimal(point.getY());
                pointY = pointY.setScale(3, BigDecimal.ROUND_HALF_UP);
                roundedPoint = new Point2D(pointX.doubleValue(), pointY.doubleValue());

				return roundedPoint.equals(roundedExpectedPoint);
			}
			
			@Override
			public void describeMismatchSafely(final Point2D point,
					final Description pointDescription)
			{
				pointDescription.appendText(roundedPoint.toString());
			}
		};	
	}
}
