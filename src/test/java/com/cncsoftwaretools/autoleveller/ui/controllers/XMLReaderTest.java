package com.cncsoftwaretools.autoleveller.ui.controllers;

import com.cncsoftwaretools.autoleveller.MajorMinor;
import com.cncsoftwaretools.autoleveller.UpdateVer;
import com.cncsoftwaretools.autoleveller.Version;
import org.junit.BeforeClass;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsEqual.equalTo;

public class XMLReaderTest
{
    private static Document xmlDoc;

    @BeforeClass
    public static void initBefore() throws ParserConfigurationException
    {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        xmlDoc = dBuilder.newDocument();
        Element root = xmlDoc.createElement("APPLICATIONS");
        Element idzero = xmlDoc.createElement("APP");
        idzero.setAttribute("id", "00");

        Element name = xmlDoc.createElement("NAME");
        name.appendChild(xmlDoc.createTextNode("Autoleveller"));
        idzero.appendChild(name);

        Element major = xmlDoc.createElement("MAJOR");
        major.appendChild(xmlDoc.createTextNode("0"));
        idzero.appendChild(major);

        Element minor = xmlDoc.createElement("MINOR");
        minor.appendChild(xmlDoc.createTextNode("8.7"));
        idzero.appendChild(minor);

        Element upNum = xmlDoc.createElement("UPDATE");
        upNum.appendChild(xmlDoc.createTextNode("1"));
        idzero.appendChild(upNum);

        Element idOne = xmlDoc.createElement("APP");
        idOne.setAttribute("id", "01");

        Element name1 = xmlDoc.createElement("NAME");
        name1.appendChild(xmlDoc.createTextNode("Autoleveller"));
        idOne.appendChild(name1);

        Element major1 = xmlDoc.createElement("MAJOR");
        major1.appendChild(xmlDoc.createTextNode("1"));
        idOne.appendChild(major1);

        Element minor1 = xmlDoc.createElement("MINOR");
        minor1.appendChild(xmlDoc.createTextNode("8.2"));
        idOne.appendChild(minor1);

        Element upNum1 = xmlDoc.createElement("UPDATE");
        upNum1.appendChild(xmlDoc.createTextNode("1"));
        idOne.appendChild(upNum1);

        root.appendChild(idzero);
        root.appendChild(idOne);
        xmlDoc.appendChild(root);
    }

    @Test
    public void xmlToVersion_invalidID()
    {
        Version version = ALGUIController.xmlToVersion(xmlDoc, "35");

        assertThat(version, equalTo(UpdateVer.IDENTITY));
    }

    @Test
    public void xmlToVersion_blankDoc() throws ParserConfigurationException
    {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Version version = ALGUIController.xmlToVersion(dBuilder.newDocument(), "35");

        assertThat(version, equalTo(UpdateVer.IDENTITY));
    }

    @Test
    public void xmlToVersion_firstID()
    {
        Version version = ALGUIController.xmlToVersion(xmlDoc, "00");

        assertThat(version.compareVersions(UpdateVer.newInstance("", 0, 8.7, 1)), is(0));
    }

    @Test
    public void xmlToVersion_secondID()
    {
        Version version = ALGUIController.xmlToVersion(xmlDoc, "01");

        assertThat(version.compareVersions(UpdateVer.newInstance("", 1, 8.2, 1)), is(0));
    }

}
