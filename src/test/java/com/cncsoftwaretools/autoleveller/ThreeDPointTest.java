package com.cncsoftwaretools.autoleveller;

import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class ThreeDPointTest
{
    @Test
    public void createPoint()
    {
        ThreeDPoint point1 = ThreeDPoint.createPoint(BigDecimal.ONE, BigDecimal.valueOf(2.1), "10");
        ThreeDPoint point2 = ThreeDPoint.createPoint(BigDecimal.ONE, BigDecimal.valueOf(2.1), "10");

        assertThat(point1.getXValue(), equalTo(BigDecimal.ONE));
        assertThat(point1.getYValue(), equalTo(BigDecimal.valueOf(2.1)));
        assertThat(point1.getZValue(), equalTo("10"));

        assertThat(point1, equalTo(point2));
        assertThat(point2, equalTo(point1));
        assertThat(point1.hashCode(), equalTo(point2.hashCode()));
        assertThat(point2.hashCode(), equalTo(point1.hashCode()));

        assertThat(point1.toString(), equalTo("ThreeDPoint{1, 2.1, 10}"));
    }
}