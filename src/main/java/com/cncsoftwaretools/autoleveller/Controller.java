/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller;

import com.cncsoftwaretools.autoleveller.ui.ALModel;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Represents the control software of the CNC machine.
 * Any element which differs between the softwares is exposed here, e.g gcode command for probing or file extension
 * Created by James Hawthorne on 23/06/2015.
 */
@SuppressWarnings("IfCanBeSwitch")
public enum Controller
{
    LINUXCNC("LinuxCNC"),
    MACH3("Mach3"),
    TURBOCNC("TurboCNC"),
    CUSTOM("Custom");

    private final String controllerStr;
    private final String defaultExt;
    private final String preProbe;
    private final String probeWord;
    private final String postProbe;
    private final String preZero;
    private final String zeroWord;
    private final String postZero;
    private final String startingParameter;
    private final String openLogCommand;
    private final String closeLogCommand;
    private final String currentZparameter;
    private static final Map<String, Controller> stringToEnum = new HashMap<>();

    private final DecimalFormat df = Autoleveller.DF;

    static
    {
        for (Controller controller : Controller.values()){stringToEnum.put(controller.toString(), controller);}
    }

    Controller(String aControllerStr) {
        if (aControllerStr.equals("LinuxCNC")){
            controllerStr = aControllerStr;
            defaultExt = "ngc";
            preProbe = "";
            probeWord = "G38.2";
            // G10 L20 P0
            postProbe = "";
            preZero ="";
            zeroWord = "G92";
            postZero = "";
            startingParameter = "#500";
            openLogCommand = "(PROBEOPEN RawProbeLog.txt)";
            closeLogCommand = "(PROBECLOSE)";
            currentZparameter = "#5422";
        }
        else if(aControllerStr.equals("Mach3"))
        {
            controllerStr = aControllerStr;
            defaultExt = "nc";
            preProbe = "";
            probeWord = "G31";
            postProbe = "";
            preZero = "";
            zeroWord = "G92";
            postZero = "";
            startingParameter = "#500";
            openLogCommand = "M40 (Begins a probe log file, when the window appears, enter a name for the log file such as \"RawProbeLog.txt\")";
            closeLogCommand = "M41 (Closes the opened log file)";
            currentZparameter = "#2002";
        }
        else if (aControllerStr.equals("TurboCNC"))
        {
            controllerStr = aControllerStr;
            defaultExt = "txt";
            preProbe = "";
            probeWord = "G31";
            postProbe = "";
            preZero = "";
            zeroWord = "G92";
            postZero = "";
            startingParameter = "#500";
            openLogCommand = "(RPF OPEN)";
            closeLogCommand = "(RPF CLOSE)";
            currentZparameter = "#2002";
        }
        else if (aControllerStr.equals("Custom"))
        {
            controllerStr = aControllerStr;
            defaultExt = "set in ALModel";
            preProbe = "set in ALModel";
            probeWord = "set in ALModel";
            postProbe = "set in ALModel";
            preZero = "set in ALModel";
            zeroWord = "set in ALModel";
            postZero = "set in ALModel";
            startingParameter = "set in ALModel";
            openLogCommand = "set in ALModel";
            closeLogCommand = "set in ALModel";
            currentZparameter = "set in ALModel";
        }
        else
        {
            assert false; //this block cannot be reached
            controllerStr = "";
            defaultExt = "";
            preProbe = "";
            probeWord = "";
            postProbe = "";
            preZero = "";
            zeroWord = "";
            postZero = "";
            startingParameter = "";
            openLogCommand = null;
            closeLogCommand = null;
            currentZparameter = null;
        }
    }

    @Override
    public String toString()
    {
        return controllerStr;
    }

    public static Optional<Controller> fromString(String controllerString){return Optional.ofNullable(stringToEnum.get(controllerString));}

    public String getDefaultExt(){return getOptionIfCustom(ALModel.CST_FILE_EXT, defaultExt);}

    public String getPreProbeCommand(){return getOptionIfCustom(ALModel.CST_PRE_PROBE_CMD, preProbe);}

    public String getProbeWord() {return getOptionIfCustom(ALModel.CST_PROBE_WORD, probeWord);}

    public String getPostProbeCommand() {return getOptionIfCustom(ALModel.CST_POST_PROBE_CMD, postProbe);}

    public String getPreZeroCommand(){return getOptionIfCustom(ALModel.CST_PRE_ZERO_CMD, preZero);}

    public String getZeroWord(){return getOptionIfCustom(ALModel.CST_ZERO_WORD, zeroWord);}

    public String getPostZeroCommand(){return getOptionIfCustom(ALModel.CST_POST_ZERO_CMD, postZero);}

    public String getOpenLogCommand(){return getOptionIfCustom(ALModel.CST_OPEN_LOG, openLogCommand);}

    public String getCloseLogCommand(){return getOptionIfCustom(ALModel.CST_CLOSE_LOG, closeLogCommand);}

    public String getCurrentZparameter(){return getOptionIfCustom(ALModel.CST_CURRENT_Z, currentZparameter);}

    public String getStartingParameter(){return getOptionIfCustom(ALModel.CST_STARTING_PARM, startingParameter);}

    private String getOptionIfCustom(String modelKey, String nonCustomOption)
    {

        ALModel model = ALModel.getInstance();

        return controllerStr.equals("Custom") ?
                Optional.ofNullable(model.readString(modelKey)).orElse("") : nonCustomOption;
    }

    public Optional<String> getProbeWordLine(BigDecimal probeDepth, BigDecimal feedRate)
    {
        if (probeWord.trim().isEmpty()){
            return Optional.empty();
        }
     
        return Optional.of(getProbeWord() + " Z" + df.format(probeDepth) + " F" + df.format(feedRate));
    }
}
