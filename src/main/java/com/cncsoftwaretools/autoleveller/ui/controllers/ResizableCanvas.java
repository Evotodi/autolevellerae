/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller.ui.controllers;

import com.cncsoftwaretools.autoleveller.Drawing;
import com.cncsoftwaretools.autoleveller.util.GraphicsContextWrapper;
import com.google.common.collect.ImmutableList;
import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import static java.util.stream.Collectors.toList;

/**
 * Created by James Hawthorne on 10/06/2017.
 *
 */
class ResizableCanvas extends Canvas
{
    private List<Drawing> drawItems = ImmutableList.of();

    ResizableCanvas()
    {
        widthProperty().addListener(evt -> draw());
        heightProperty().addListener(evt -> draw());
    }

    void draw()
    {
        GraphicsContextWrapper gc = new GraphicsContextWrapper(getGraphicsContext2D());
        gc.clearRect(0, 0, getWidth(), getHeight());
        gc.setFill(Color.BEIGE);
        gc.fillRect(0, 0, getWidth(), getHeight());

        if (!drawItems.isEmpty()) {
            Rectangle combinedArea = combineRects(
                    drawItems.stream().map(Drawing::originalSize).collect(toList())).orElse(new Rectangle());
            double ratio = getRatio(new Rectangle(getWidth(), getHeight()), combinedArea);

            drawItems.forEach(drawItem -> {
                final double xCombinedDiff = (drawItem.originalSize().getX() - combinedArea.getX());
                final double yCombinedDiff = (drawItem.originalSize().getY() - combinedArea.getY());

                drawItem.getDrawingInstructions().forEach(function -> function.apply(gc, ratio,
                        new Point2D(xCombinedDiff, yCombinedDiff)));
            });
        }
    }

    void setDrawItems(ImmutableList<Drawing> drawItems)
    {
        this.drawItems = drawItems;
    }

    private Optional<Rectangle> combineRects(List<Shape> shapes)
    {
        Function<Shape, Rectangle> shapeToRect = shape -> {
            Bounds lb = shape.getLayoutBounds();
            return new Rectangle(lb.getMinX(), lb.getMinY(), lb.getWidth(), lb.getHeight());
        };

        Optional<Shape> shapeUnionOpt = shapes.stream().reduce(Shape::union);

        return shapeUnionOpt.map(shapeToRect);
    }

    private double getRatio(Rectangle drawPaneRect, Rectangle modelRect)
    {
        double widthRatio = drawPaneRect.getWidth() / modelRect.getWidth();
        double heightRatio = drawPaneRect.getHeight() / modelRect.getHeight();

        // if the rect wont fit in the draw pane width wise return height ratio otherwise return width ratio
        return modelRect.getHeight() * widthRatio > drawPaneRect.getHeight() ? heightRatio : widthRatio;
    }

    @Override
    public boolean isResizable()
    {
        return true;
    }

    @Override
    public double prefWidth(double height)
    {
        return getWidth();
    }

    @Override
    public double prefHeight(double width)
    {
        return getHeight();
    }

    public void clear()
    {
        GraphicsContextWrapper gc = new GraphicsContextWrapper(getGraphicsContext2D());
        gc.clearRect(0, 0, getWidth(), getHeight());
    }
}
