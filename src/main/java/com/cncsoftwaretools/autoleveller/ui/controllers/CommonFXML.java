/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller.ui.controllers;

import com.cncsoftwaretools.autoleveller.*;
import com.cncsoftwaretools.autoleveller.ui.ALModel;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;

import java.io.*;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.io.FilenameUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.cncsoftwaretools.autoleveller.Autoleveller.getLogger;

/**
 * Created by James Hawthorne on 04/03/2017.
 *
 */
public class CommonFXML
{
    private static final Logger LOGGER = getLogger(CommonFXML.class.getName()).orElse(Logger.getAnonymousLogger());
    private final ALModel model = ALModel.getInstance();

    @FXML
    public void chooseOGF()
    {
        FileChooser fc = createFileChooser("Locate Original GCode");

        Optional<File> fileOptional = Optional.ofNullable(fc.showOpenDialog(null));
        fileOptional.ifPresent(ogfFile -> {
            model.writeDirectory(ogfFile.getParent());
            Optional<OGF> ogfOpt = OGF.newInstance(ogfFile.getAbsolutePath());
            ogfOpt.ifPresent(model::setOGF);
            if (!ogfOpt.isPresent()){
                LOGGER.log(Level.WARNING, "OGF cannot be read. Z values are never negative?");
                Alert ogfAlert = new Alert(Alert.AlertType.ERROR, "Cannot load OGF. Autoleveller assumes that " +
                        "\nnegative Z values move the tool below the surface of the material and therefore " +
                        "make a cut. An OGF cannot be read if there are no negative Z's. " +
                        "\nMaybe the file you are trying to read is never negative, or the OGF is otherwise invalid?");
                ogfAlert.setHeaderText("OGF Load Error");

                ogfAlert.showAndWait();
                model.setOGF(null);
            }

        });
        AtomicReference<Boolean> loadedMesh = new AtomicReference<>(false);
        File ogfFile = new File(model.getOGF().getFilename());
        String ogfFileBasename = FilenameUtils.removeExtension(ogfFile.getAbsolutePath());
        File meshFile = new File(ogfFileBasename + ".msh");
        if(meshFile.exists()){
            Optional.ofNullable(model.getOGF()).ifPresent(ogf -> showLoadMeshDialog().ifPresent(buttonReturn -> {
                if (buttonReturn.getButtonData().equals(ButtonBar.ButtonData.YES)){
                    setMeshFromOGF(ogf);
                    loadMesh(Optional.of(meshFile));
                    loadedMesh.set(true);
                }
            }));

        }

        if(!loadedMesh.get()){
            Optional.ofNullable(model.getOGF()).ifPresent(ogf -> showMeshDialog().ifPresent(buttonReturn -> {
                if (buttonReturn.getButtonData().equals(ButtonBar.ButtonData.YES)){
                    setMeshFromOGF(ogf);
                }
            }));
        }

    }

    private void setMeshFromOGF(OGF ogf)
    {
        Rectangle ogfRect = ogf.getSize();
        Mesh.Builder meshBuilder = new Mesh.Builder(ogf.getUnits(), model.readController(), BigDecimal.valueOf(ogfRect.getX()),
                BigDecimal.valueOf(ogfRect.getY()), BigDecimal.valueOf(ogfRect.getWidth()), BigDecimal.valueOf(ogfRect.getHeight()));

        model.setMesh(buildMeshOptionsFromModel(meshBuilder));
    }

    private Mesh buildMeshOptionsFromModel(Mesh.Builder meshBuilder)
    {
        try {
            meshBuilder.buildXYFeed(model.readDouble(ALModel.XY_FEED_KEY));
            meshBuilder.buildZFeed(model.readDouble(ALModel.Z_FEED_KEY));
            meshBuilder.buildDepth(model.readDouble(ALModel.PROBE_DEPTH));
            meshBuilder.buildClearance(model.readDouble(ALModel.PROBE_CLEARANCE));
            meshBuilder.buildSpacing(model.readDouble(ALModel.PROBE_SPACING));
            meshBuilder.buildSafeHeight(model.readDouble(ALModel.SAFE_HEIGHT));
            meshBuilder.buildInsets(model.readDouble(ALModel.LEFT_INSET), model.readDouble(ALModel.RIGHT_INSET),
                    model.readDouble(ALModel.TOP_INSET), model.readDouble(ALModel.BOTTOM_INSET));
        }
        catch (IllegalArgumentException iae){
            LOGGER.log(Level.WARNING, "Cannot access ALModel mesh options, to set Mesh, using default options instead");
            return meshBuilder.build();
        }

        return meshBuilder.build();
    }

    @FXML
    public void chooseRPF()
    {
        FileChooser fc = createFileChooser("Locate RPF");

        Optional<File> fileOptional = Optional.ofNullable(fc.showOpenDialog(null));
        fileOptional.ifPresent((File rpfFile) -> {
            model.writeDirectory(rpfFile.getParent());
            Optional<RPF> rpfOpt = RPF.newInstance(rpfFile.getAbsolutePath());
            rpfOpt.ifPresent(model::setRPF);
            if (!rpfOpt.isPresent()){
                Alert rpfAlert = new Alert(Alert.AlertType.ERROR, "Cannot load RPF because of the following possible reasons:" +
                        "\nThe chosen file format is incorrect" +
                        "\nThe number of points per row is not the same for each row");
                rpfAlert.setHeaderText("RPF Load Error");

                rpfAlert.showAndWait();
                model.setRPF(null);
            }
        });
    }

    FileChooser createFileChooser(String title)
    {
        FileChooser fc = new FileChooser();
        fc.setTitle(title);
        ALModel model = ALModel.getInstance(); //this line trys to ensure the model is copied to the JavaFX thread

        try {
            String defaultPath = model.readDirectory();
            boolean pathExists = Files.exists(Paths.get(defaultPath));

            //only set the initial directory if 'defaultPath is not null
            if (pathExists) {
                fc.setInitialDirectory(new File(defaultPath));
            } else {
                LOGGER.log(Level.INFO, "default path is null");
                fc.setInitialDirectory(new File(System.getProperty("user.home")));
            }

            return fc;
        }
        catch (Exception e)
        {
            LOGGER.log(Level.WARNING, "error thrown creating FileChooser");
            return fc;
        }
    }

    private Optional<ButtonType> showMeshDialog()
    {
        Alert meshAlert = new Alert(Alert.AlertType.CONFIRMATION);
        meshAlert.setTitle("Mesh Settings");
        meshAlert.setHeaderText("Change mesh settings");
        ButtonType yesBtn = new ButtonType("Yes", ButtonBar.ButtonData.YES);
        ButtonType noBtn = new ButtonType("No", ButtonBar.ButtonData.NO);
        meshAlert.getButtonTypes().setAll(yesBtn, noBtn);

        meshAlert.setContentText("Do you want to change the mesh settings based on your OGF?");
        return meshAlert.showAndWait();
    }

    private Optional<ButtonType> showLoadMeshDialog()
    {
        Alert meshAlert = new Alert(Alert.AlertType.CONFIRMATION);
        meshAlert.setTitle("Load Mesh Settings");
        meshAlert.setHeaderText("Saved mesh found");
        ButtonType yesBtn = new ButtonType("Yes", ButtonBar.ButtonData.YES);
        ButtonType noBtn = new ButtonType("No", ButtonBar.ButtonData.NO);
        meshAlert.getButtonTypes().setAll(yesBtn, noBtn);

        meshAlert.setContentText("Do you want to load the saved mesh for the OGF?");
        return meshAlert.showAndWait();
    }

    @FXML
    public void saveMesh() {
        if(model.getOGF() == null){
            Alert ogfAlert = new Alert(Alert.AlertType.ERROR, "OGF is not loaded");
            ogfAlert.setHeaderText("Save Mesh Error");
            ogfAlert.showAndWait();
            return;
        }
        File ogfFile = new File(model.getOGF().getFilename());
        String ogfFileBasename =  FilenameUtils.getBaseName(ogfFile.getName());

        FileChooser fc = createFileChooser("Save Mesh");
        fc.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Mesh Files", "*.msh")
        );
        fc.setInitialFileName(ogfFileBasename);
        Optional<File> fileOptional = Optional.ofNullable(fc.showSaveDialog(null));
        fileOptional.ifPresent((File meshFile) -> {
            LOGGER.log(Level.INFO, meshFile.getAbsolutePath());
            try {
                FileWriter fileWriter = new FileWriter(meshFile.getAbsolutePath());
                fileWriter.write(JSONObject.toJSONString(getMeshArray()));
                fileWriter.close();
            }catch (IOException e) {
                e.printStackTrace();
                Alert ogfAlert = new Alert(Alert.AlertType.ERROR, "Error occurred saving the mesh file.");
                ogfAlert.setHeaderText("Save Mesh Error");
                ogfAlert.showAndWait();
            }
        });

        LOGGER.log(Level.INFO, JSONObject.toJSONString(getMeshArray()));

    }

    @FXML
    public void loadMesh(Optional<File> existingMeshFile) {
        if(existingMeshFile.isPresent()){
            parseSavedMesh(existingMeshFile.get().getAbsolutePath());
            return;
        }
        if(model.getOGF() == null){
            Alert ogfAlert = new Alert(Alert.AlertType.ERROR, "OGF is not loaded");
            ogfAlert.setHeaderText("Load Mesh Error");
            ogfAlert.showAndWait();
            return;
        }

        FileChooser fc = createFileChooser("Locate Saved Mesh");
        Optional<File> fileOptional = Optional.ofNullable(fc.showOpenDialog(null));
        fileOptional.ifPresent((File meshFile) -> {
            LOGGER.log(Level.INFO, meshFile.getAbsolutePath());
            parseSavedMesh(meshFile.getAbsolutePath());
        });

    }

    public Map<String, Object> getMeshArray() {
        Map<String, Object> meshArray = new LinkedHashMap<>();
        meshArray.put("xValue", model.getMesh().getInsetlessSize().get(Mesh.Rect.X));
        meshArray.put("yValue", model.getMesh().getInsetlessSize().get(Mesh.Rect.Y));
        meshArray.put("xLength", model.getMesh().getInsetlessSize().get(Mesh.Rect.WIDTH));
        meshArray.put("yLength", model.getMesh().getInsetlessSize().get(Mesh.Rect.HEIGHT));
        meshArray.put("zFeed", model.getMesh().getzFeed());
        meshArray.put("xyFeed", model.getMesh().getXyFeed());
        meshArray.put("probeDepth", model.getMesh().getProbeDepth());
        meshArray.put("probeClearance", model.getMesh().getProbeClearance());
        meshArray.put("pointSpacing", model.getMesh().getPointSpacing());
        meshArray.put("safeHeight", model.getMesh().getSafeHeight());
        meshArray.put("insetLeft", model.getMesh().getInset(Mesh.Inset.LEFT));
        meshArray.put("insetRight", model.getMesh().getInset(Mesh.Inset.RIGHT));
        meshArray.put("insetTop", model.getMesh().getInset(Mesh.Inset.TOP));
        meshArray.put("insetBottom", model.getMesh().getInset(Mesh.Inset.BOTTOM));
        meshArray.put("units", model.getOGF().getUnits().toString());
        meshArray.put("controller", model.getMesh().getController().toString());

        return meshArray;
    }

    public void parseSavedMesh(String meshFile) {
        JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader(meshFile))
        {
            JSONObject json = (JSONObject) jsonParser.parse(reader);

            Mesh.Builder builder = new Mesh.Builder(
                model.getMesh().getUnits(),
                model.getMesh().getController(),
                BigDecimal.valueOf((Double) json.get("xValue")),
                BigDecimal.valueOf((Double) json.get("yValue")),
                BigDecimal.valueOf((Double) json.get("xLength")),
                BigDecimal.valueOf((Double) json.get("yLength"))
            );

            builder.buildInsets((Double) json.get("insetLeft"), (Double) json.get("insetRight"), (Double) json.get("insetTop"), (Double) json.get("insetBottom"));
            builder.buildSpacing((Double) json.get("pointSpacing"));
            builder.buildClearance((Double) json.get("probeClearance"));
            builder.buildDepth((Double) json.get("probeDepth"));
            builder.buildZFeed((Double) json.get("zFeed"));
            builder.buildXYFeed((Double) json.get("xyFeed"));
            builder.buildSafeHeight((Double) json.get("safeHeight"));

            model.setMesh(builder.build());


        } catch (FileNotFoundException e) {
            Alert ogfAlert = new Alert(Alert.AlertType.ERROR, "Mesh file not found");
            ogfAlert.setHeaderText("File Not Found");
            ogfAlert.showAndWait();
            e.printStackTrace();
        } catch (ParseException | IOException e) {
            Alert ogfAlert = new Alert(Alert.AlertType.ERROR, "Could not parse the mesh file");
            ogfAlert.setHeaderText("Parse Error");
            ogfAlert.showAndWait();
            e.printStackTrace();
        }
    }
}

