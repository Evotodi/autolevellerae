/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller;

import com.google.inject.Inject;
import javafx.geometry.Point2D;

import java.math.BigDecimal;

/**
 * A ThreeDPoint represents the X, Y, and Z values of a point on a 3 axis system.
 * X and Y are always of BigDecimal type whereas Z is a String since Z could be a gcode variable such as #102
 * Created by James Hawthorne on 11/06/2015.
 */
public class ThreeDPoint
{
    private final BigDecimal xValue;
    private final BigDecimal yValue;
    private final String zValue;

    private ThreeDPoint(BigDecimal thisXValue, BigDecimal thisYValue, String thisZValue)
    {
        xValue = thisXValue;
        yValue = thisYValue;
        zValue = thisZValue;
    }

    public static ThreeDPoint createPoint(BigDecimal thisXValue, BigDecimal thisYValue, String thisZValue)
    {
        return new ThreeDPoint(thisXValue, thisYValue, thisZValue);
    }

    public BigDecimal getXValue()
    {
        return xValue;
    }

    public BigDecimal getYValue()
    {
        return yValue;
    }

    public String getZValue()
    {
        return zValue;
    }

    /**
     * Returns a Pair where the first value is this X and the send value is this Y
     */
    public Point2D get2DPoint()
    {
        return new Point2D(xValue.doubleValue(), yValue.doubleValue());
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ThreeDPoint that = (ThreeDPoint) o;

        if (!xValue.equals(that.xValue)) return false;
        //noinspection SuspiciousNameCombination
        if (!yValue.equals(that.yValue)) return false;
        //noinspection RedundantIfStatement
        if (!zValue.equals(that.zValue)) return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = xValue.hashCode();
        result = 31 * result + yValue.hashCode();
        result = 31 * result + zValue.hashCode();
        return result;
    }

    @Override
    public String toString()
    {
        return "ThreeDPoint{" + xValue +", " + yValue + ", " + zValue + '}';
    }
}
