/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller.reader;

/**
 * A Word Group tells us what group a word belongs to. If a Word is in anything other than NOGROUP it can be overwritten
 * in the state by another word of the same WordGroup, otherwise it is a one-shot non-recordable Word.
 * Created by James Hawthorne on 20/08/2015.
 */
public enum WordGroup
{
    NOGROUP,
    FEEDGROUP, SPEEDGROUP,
    MOTIONGROUP, UNITSGROUP, DISTANCEGROUP, CUTTERCOMP, TOOLLENGTHOFFSET, PATHCONTMODE, TOOLGROUP, ARCMODE,
    XAXISGROUP, YAXISGROUP, ZAXISGROUP, // Current Axis need to be persisted and are therefore give their own group
}
