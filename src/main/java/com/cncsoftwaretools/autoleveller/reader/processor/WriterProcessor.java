/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller.reader.processor;

import com.cncsoftwaretools.autoleveller.ALWriter;
import com.cncsoftwaretools.autoleveller.reader.GCodeState;
import com.google.common.collect.ImmutableList;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.cncsoftwaretools.autoleveller.Autoleveller.getLogger;

/**
 * Implementation of GCodeProcessor where each line is written to the given file then passes the state to the next processor
 * Created by James Hawthorne on 17/09/2015.
 */
public class WriterProcessor extends GCodeInterpreter
{
    private final Optional<GCodeProcessor> processor;
    private final String filename;
    private final List<String> bufferedLines = new ArrayList<>();
    private static final long BUFFERSIZE = 100;
    private static final Optional<Logger> LOGGER = getLogger(WriterProcessor.class.getName());

    private WriterProcessor(Optional<GCodeProcessor> processor, String filename, boolean append) throws IOException
    {
        //create a new file only if append is set
        if (!append){Files.newBufferedWriter(Paths.get(filename), StandardCharsets.UTF_8);}

        this.processor = processor;
        this.filename = filename;
    }

    public static GCodeProcessor newInstance(Optional<GCodeProcessor> processor, String fileName, boolean append) throws IOException
    {
        return new WriterProcessor(processor, fileName, append);
    }

    @Override
    public List<GCodeState> processState(GCodeState currentState)
    {
        bufferedLines.add(currentState.getRawLine());

        if (bufferedLines.size() >= BUFFERSIZE) {
            ALWriter.useALWriter(createPrintWriterFromFile().get(), this::writeFromBuffer);
        }

        //end of chain, no need to return anything but an empty list
        return ImmutableList.of();
    }

    @Override
    public Optional<GCodeProcessor> nextProcessor()
    {
        return processor;
    }

    private Optional<PrintWriter> createPrintWriterFromFile()
    {
        try {
            return Optional.of(new PrintWriter(new BufferedWriter(new FileWriter(filename, true))));
        } catch (IOException e) {
            LOGGER.ifPresent(logger -> logger.log(Level.SEVERE, "Error creating writer from " + filename));
            return Optional.empty();
        }
    }

    private void writeFromBuffer(ALWriter alWriter)
    {
        bufferedLines.forEach(alWriter::writeGCODELine);
        bufferedLines.clear();
    }

    @Override
    public void finish()
    {
        //finish writing from buffer
        ALWriter.useALWriter(createPrintWriterFromFile().get(), this::writeFromBuffer);
    }
}
