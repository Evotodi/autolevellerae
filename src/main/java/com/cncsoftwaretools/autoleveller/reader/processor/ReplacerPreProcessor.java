/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller.reader.processor;

import com.cncsoftwaretools.autoleveller.reader.GCodeState;
import com.google.common.collect.ImmutableMap;
import com.sksamuel.diffpatch.DiffMatchPatch;

import java.util.LinkedList;

import static java.util.stream.Collectors.joining;

/**
 * Designed to replace a regular expression found in the GCodeState with an alternative String
 * This gives us the chance to remove or improve erroneus Strings found in the OGF such as #
 * Created by James Hawthorne on 26/11/2015.
 */
public class ReplacerPreProcessor
{
    private static final ImmutableMap<String, String> replaceMap = ImmutableMap.of(
            "^%$", "(removed % symbol)",
            "[\\.,](?=\\D)|\\.$", "", // remove trailing dots
            "(?<=\\D)[\\.,]", "0."); // remove leading dots;

    public static ReplacerPreProcessor newInstance()
    {
        return new ReplacerPreProcessor();
    }

    /**
     * If the state contains any regex in the keys of the replaceMap, replace it with the accociated value
     * @param state the GCodeState object with may or may not include any of the regex's in replaceMap
     * @return a GCodeState with the replacement text or the state as is if no regex's were found
     */
    GCodeState replace(GCodeState state)
    {
        return GCodeState.newInstance(state.getPreviousState(), normalizeString(state.getRawLine()));
    }

    public static String normalizeString(String origStr)
    {
        String commentlessLine = GCodeState.removeComments(origStr.trim());
        LinkedList<DiffMatchPatch.Diff> diffs = new DiffMatchPatch().diff_main(origStr, commentlessLine);
        String replacementText = commentlessLine;

        for (String searchRegex : replaceMap.keySet()){
            replacementText = replacementText.replaceAll(searchRegex, replaceMap.get(searchRegex));
        }

        //add comments back to line after making changes
        return replacementText + diffs.stream()
                .filter(diff -> diff.operation == DiffMatchPatch.Operation.DELETE)
                .map(diff -> diff.text)
                .collect(joining(""));
    }
}
