/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller.reader.processor;

import com.cncsoftwaretools.autoleveller.ALTextBlockFunctions;
import com.cncsoftwaretools.autoleveller.Autoleveller;
import com.cncsoftwaretools.autoleveller.reader.GCodeState;
import com.cncsoftwaretools.autoleveller.reader.Word;
import com.cncsoftwaretools.autoleveller.reader.WordGroup;
import com.cncsoftwaretools.autoleveller.util.Arc;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;
import javafx.geometry.Point2D;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.cncsoftwaretools.autoleveller.util.Arc.*;

/**
 * Segments any arc which is > the segment Length
 * Segmented arcs have the same length, and location as the original arc
 * they are just segmented into several smaller arcs so they can be levelled more regularly.
 * Created by James Hawthorne on 25/03/2016.
 */
public class ArcSegmentProcessor extends GCodeInterpreter
{
    private final BigDecimal segmentLength;
    private final Optional<GCodeProcessor> processor;
    private final String formattedSegmentLength;

    private Optional<GCodeState> lastProcessedState = Optional.empty();

    private static final Optional<Logger> LOGGER = Autoleveller.getLogger(ArcSegmentProcessor.class.getName());

    private ArcSegmentProcessor(Optional<GCodeProcessor> processor, double segmentLength)
    {
        this.segmentLength = new BigDecimal(segmentLength);
        this.processor = processor;
        this.formattedSegmentLength = this.segmentLength.setScale(ALTextBlockFunctions.SCALE, BigDecimal.ROUND_HALF_UP)
                .stripTrailingZeros()
                .toPlainString();
    }

    /**
     * Creates a new instance of an ArcSegmentProcessor
     *
     * @param processor     The processor to chain after this has finished
     * @param segmentLength This will be the maximum length of an arc segment
     */
    public static ArcSegmentProcessor newInstance(Optional<GCodeProcessor> processor, double segmentLength)
    {
        return new ArcSegmentProcessor(processor, segmentLength);
    }

    @Override
    public List<GCodeState> processState(final GCodeState originalState)
    {
        GCodeState nxtToProcess = GCodeState.newInstance(lastProcessedState, originalState.getRawLine());
        if (!validArcCheck(nxtToProcess)) {
            lastProcessedState = Optional.of(nxtToProcess);
            return ImmutableList.of(lastProcessedState.get());
        }

        Arc arc = createArcFromState(nxtToProcess).get();
        ImmutableList.Builder<GCodeState> segments = ImmutableList.builder();
        Point2D centrePoint = originalState.wordBeginningOnCurrentLine('R').isPresent() ?
                getCentreFromRadius(arc.getStartPoint(), arc.getEndPoint(), arc.getDirection(), originalState.wordValueOnCurrentLineGetter()).get() :
                getCentreFromOffsets(originalState, arc.getStartPoint(), originalState.wordValueOnCurrentLineGetter()).get();

        while (arc.arcLength().doubleValue() > segmentLength.doubleValue()) {
            Optional<GCodeState> nextState = nextState(nxtToProcess);
            if (nextState.isPresent()) {
                segments.add(nextState.get());
                arc = Arc.newInstance(nextState.get().get2DPoint().get(), originalState.get2DPoint().get(), centrePoint, arc.getDirection());
                nxtToProcess = GCodeState.newInstance(nextState, arcAsGCodeString(arc, originalState));
            }
        }
        lastProcessedState = Optional.of(nxtToProcess);
        segments.add(nxtToProcess);
        return segments.build();
    }

    @SuppressWarnings("SimplifiableIfStatement")
    private boolean validArcCheck(GCodeState state)
    {
        Optional<BigDecimal> gcodeValue = state.wordValueOnCurrentLineGetter().apply('G');
        if (gcodeValue.isPresent() && (gcodeValue.get().doubleValue() == 2 || gcodeValue.get().doubleValue() == 3)) {
            if (!isValidArcState(state)) {
                LOGGER.ifPresent(logger -> logger.log(Level.WARNING, "GCode state is not a valid arc " + state.getRawLine()));
                return false;
            }
            return true;
        }

        return false;
    }

    @Override
    public Optional<GCodeProcessor> nextProcessor() {return processor;}

    /**
     * If {@code state} is an arc and it needs segmenting, return the next segment, otherwise return that state as is
     *
     * @param state the the next GCodeState object
     * @return the next GCodeState. Segmented or not
     */
    @VisibleForTesting
    Optional<GCodeState> nextState(GCodeState state)
    {
        if (!isValidArcState(state)) { //this method has package visibility and is not normally called externally
            LOGGER.ifPresent(logger -> logger.log(Level.WARNING, "GCode state is not a valid arc " + state.getRawLine()));
        }

        Optional<Arc> arc = segmentedArcFromState(state);
        if (!arc.isPresent()) {return Optional.empty();} // if we werent able to create an arc, return empty()

        String gcodeArc = arcAsGCodeString(arc.get(), state) + " ;segmented arc. Max segment length set to " + formattedSegmentLength;

        return Optional.of(GCodeState.newInstance(state.getPreviousState(), gcodeArc));
    }

    @VisibleForTesting
    String arcAsGCodeString(Arc arc, GCodeState state)
    {
        Optional<Word> absArcWordOpt = state.getModalWordByGroup(WordGroup.ARCMODE);
        Optional<Word> zWord = state.wordBeginningOnCurrentLine('Z');
        Optional<Word> fWord = state.wordBeginningOnCurrentLine('F');

        String direction = arc.getDirection().equals(Arc.Direction.CW) ? "G2" : "G3";
        String endPoints = " X" + Autoleveller.DF.format(arc.getEndPoint().getX()) + " Y" +
                Autoleveller.DF.format(arc.getEndPoint().getY());
        String centre = absArcWordOpt.isPresent() && absArcWordOpt.get().getWordPair().getSecond().equals(BigDecimal.valueOf(90.1)) ?
                " I" + Autoleveller.DF.format(arc.getCentrePoint().getX()) + " J" +
                        Autoleveller.DF.format(arc.getCentrePoint().getY()) :
                " I" + Autoleveller.DF.format(arc.getCentrePoint().getX() - arc.getStartPoint().getX()) + " J" +
                        Autoleveller.DF.format(arc.getCentrePoint().getY() - arc.getStartPoint().getY());

        return direction +
                endPoints +
                (zWord.isPresent() ? " " + zWord.get().asString() : "") + //if Z exists, put it back
                centre +
                (fWord.isPresent() ? " " + fWord.get().asString() : ""); //if F exists, put it back;
    }

    @VisibleForTesting
    Optional<Arc> segmentedArcFromState(GCodeState state)
    {
        Optional<Arc> completeArcOpt = createArcFromState(state);

        if (!completeArcOpt.isPresent()) {return Optional.empty();}

        Arc completeArc = completeArcOpt.get();
        Point2D endPoint = Arc.endPointForArcLength(completeArc.getStartPoint(), completeArc.getCentrePoint(),
                segmentLength.doubleValue(), completeArc.getDirection());

        return Optional.of(Arc.newInstance(completeArc.getStartPoint(), endPoint, completeArc.getCentrePoint(),
                completeArc.getDirection()));
    }
}

