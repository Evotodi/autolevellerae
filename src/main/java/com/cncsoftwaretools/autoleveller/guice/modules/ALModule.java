/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller.guice.modules;

import com.cncsoftwaretools.autoleveller.Units;
import com.cncsoftwaretools.autoleveller.guice.factories.RPF3DModelProvider;
import com.cncsoftwaretools.autoleveller.ui.drawables.Axis3dModelGroup;
import com.cncsoftwaretools.autoleveller.ui.drawables.FocusedTransformableCamera;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.assistedinject.Assisted;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import com.sun.javafx.collections.ObservableFloatArrayImpl;
import javafx.collections.ObservableFloatArray;
import javafx.scene.DepthTest;
import javafx.scene.Group;
import javafx.scene.PerspectiveCamera;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.CullFace;
import javafx.scene.shape.DrawMode;
import javafx.scene.shape.MeshView;
import javafx.scene.shape.TriangleMesh;
import javafx.scene.transform.Rotate;

/**
 *
 * Created by James Hawthorne on 18/09/2016.
 */
public class ALModule extends AbstractModule
{

    @Override
    protected void configure()
    {
        bind(ObservableFloatArray.class).to(ObservableFloatArrayImpl.class);

        install(new FactoryModuleBuilder()
                .build(RPF3DModelProvider.class));
    }

    @Provides PerspectiveCamera provideCamera(){return new PerspectiveCamera(true);}
    @Provides @FocusedTransformableCamera.XRotate Rotate provideXRotate(){return  new Rotate(50, Rotate.X_AXIS);}
    @Provides @FocusedTransformableCamera.ZRotate Rotate provideZRotate(){return  new Rotate(45, Rotate.Z_AXIS);}

    @Provides
    MeshView providesMeshView(PhongMaterial material, TriangleMesh triangleMesh)
    {
        MeshView emptyMesh = new MeshView();

        emptyMesh.setMaterial(material);
        emptyMesh.setMesh(triangleMesh);
        emptyMesh.setCullFace(CullFace.NONE);
        emptyMesh.setDrawMode(DrawMode.FILL);
        emptyMesh.setDepthTest(DepthTest.ENABLE);

        return emptyMesh;
    }
}
