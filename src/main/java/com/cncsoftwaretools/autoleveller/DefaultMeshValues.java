/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Holds the global default values for the probe settings
 * Created by James Hawthorne on 18/03/2016.
 */
public enum DefaultMeshValues
{
    MM(Units.MM),
    INCHES(Units.INCHES);

    private static final int SCALE = 5;
    private final Units units;

    DefaultMeshValues(Units units){this.units = units;}

    public BigDecimal getZFeed(){
        return (units.equals(Units.MM) ? BigDecimal.valueOf(100) : BigDecimal.valueOf(5))
                .setScale(SCALE, RoundingMode.HALF_EVEN);
    }

    public BigDecimal getXYFeed(){
        return (units.equals(Units.MM) ? BigDecimal.valueOf(600) : BigDecimal.valueOf(35))
                .setScale(SCALE, RoundingMode.HALF_EVEN);
    }

    public BigDecimal getProbeDepth(){
        return (units.equals(Units.MM) ? BigDecimal.valueOf(-1) : BigDecimal.valueOf(-0.0625))
                .setScale(SCALE, RoundingMode.HALF_EVEN);
    }

    public BigDecimal getProbeClearance(){
        return (units.equals(Units.MM) ? BigDecimal.valueOf(2) : BigDecimal.valueOf(0.0787))
                .setScale(SCALE, RoundingMode.HALF_EVEN);
    }

    public BigDecimal getPointSpacing(){
        return (units.equals(Units.MM) ? BigDecimal.TEN : BigDecimal.valueOf(0.375))
                .setScale(SCALE, RoundingMode.HALF_EVEN);
    }

    public BigDecimal getSafeHeight(){
        return (units.equals(Units.MM) ? BigDecimal.valueOf(25) : BigDecimal.valueOf(1.5))
                .setScale(SCALE, RoundingMode.HALF_EVEN);
    }

    // for the foreseeable future the inset will always be 0 but you may want a default inset value based
    // on the specific inset. For this reason inset is provided as a parameter.
    @SuppressWarnings("unused")
    public Double getInset(Mesh.Inset inset){return 0.0;}
}
